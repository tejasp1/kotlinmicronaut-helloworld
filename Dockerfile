FROM java:jre-alpine

EXPOSE 8080

COPY ./build/libs/movie-service-0.1.jar movie-service.jar

ENTRYPOINT ["java", "-jar", "movie-service.jar"]