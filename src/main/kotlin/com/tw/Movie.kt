package com.tw

import javax.persistence.Entity
import javax.persistence.Id

@Entity
class Movie constructor (){

    lateinit var name:String

    @Id
    lateinit var imdbId:String

    constructor(name:String,imdbId:String) : this() {
        this.name = name
        this.imdbId = imdbId
    }

}
