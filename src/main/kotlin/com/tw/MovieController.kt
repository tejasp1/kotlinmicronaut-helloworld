package com.tw

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import javax.inject.Inject

@Controller("/movies")
class MovieController constructor(){

    @Inject
    lateinit var movieRepository : MovieRepository

    constructor(movieRepository: MovieRepository) : this() {
        this.movieRepository = movieRepository
    }

    @Get("/")
    fun findAll() : MutableList<Movie>? {
        return movieRepository.findAll()
    }

    @Get("/{imdbId}")
    fun findByImdbId(@PathVariable imdbId:String) : Movie?{
        return movieRepository.findByImdbId(imdbId)
    }


}
