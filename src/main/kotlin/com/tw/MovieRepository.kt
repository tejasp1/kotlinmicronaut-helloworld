package com.tw

import io.micronaut.spring.tx.annotation.Transactional
import javax.inject.Singleton
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Singleton
open class MovieRepository(@PersistenceContext private val manager : EntityManager) {

    @Transactional
    open fun findAll()=manager.createQuery("from Movie", Movie::class.java).resultList

    @Transactional
    open fun findByImdbId(imdbId:String): Movie? {
        return manager.find(Movie::class.java, imdbId)
    }
}