package com.tw

import io.micronaut.core.type.Argument
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.annotation.MicronautTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import javax.inject.Inject

@MicronautTest
class MovieControllerTest {
    @Inject
    @field:Client("/")
    lateinit var client : HttpClient

    @Test
    fun `hello World Greeting Test`(){
        val request = HttpRequest.GET<Any>("/movies")

        val result = client.toBlocking().
                retrieve(request, Argument.of(List::class.java,Movie::class.java))

        assertEquals(2,result.size)
    }

    @Test
    fun `hello World Greeting Test2`(){
        val result = client.toBlocking().
                exchange("/movies",Movie::class.java)

        assertEquals(HttpStatus.OK,result.status)
    }

    @Test
    fun `test find movie vy imdbId`(){
        val result = client.toBlocking().
                exchange("/movies/12345",Movie::class.java)

        assertEquals(HttpStatus.OK,result.status)
    }

}