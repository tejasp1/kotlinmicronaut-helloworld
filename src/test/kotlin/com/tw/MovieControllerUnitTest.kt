package com.tw

import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class MovieControllerUnitTest {


    @MockK
    lateinit var movieRepository:MovieRepository

    @InjectMockKs
    var movieController = MovieController()

    @BeforeEach
    fun setUp() = MockKAnnotations.init(this)

    @Test
    fun testFindAll(){
        val movie = Movie("DDLJ","12345")
        every { movieRepository.findAll() } returns listOf(movie)

        val result : List<Movie>? = movieController.findAll()

        assertEquals(1, result?.size)
        assertEquals("DDLJ", result?.first()?.name)
        assertEquals("12345", result?.first()?.imdbId)
    }

    @Test
    fun testFindByImdbId(){
        val movie = Movie("DDLJ","12345")
        val imdbId = "12345"
        every { movieRepository.findByImdbId(imdbId) } returns movie

        val result = movieController.findByImdbId(imdbId)

        assertEquals("DDLJ", result?.name)
        assertEquals("12345", result?.imdbId)
    }


}