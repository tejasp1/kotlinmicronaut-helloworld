package com.tw

import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.annotation.MicronautTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import javax.inject.Inject

@MicronautTest
class WelcomeControllerTest {

    @Inject
    @field:Client("/")
    lateinit var client : HttpClient


    @Test
    fun `hello World Greeting Test`(){
        val result = client.toBlocking().retrieve("/hello")

        assertEquals("Hello World!!", result)
    }

}